$(document).ready(function(){
  $('.search-area').on('click',function(){
    $('.search-icon').hide();
    $('#search').show().focus();
  });

  $('#search').on('keypress',function(e){
    if(e.keyCode == 13){
      $(this).val('').hide();
      $('.search-icon').show();
    }
  });

  $('#search').on('focusout blur',function(e){
      $(this).val('').hide();
      $('.search-icon').show();
  });

});
